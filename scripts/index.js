$(document).ready(function () {
	$('#map-canvas').css({ width: $(window).width(),  height: $(window).height()-50 });  
	$("#gz-list").panel( "open");    		
	$('#section-gz').css({ height: $(window).height()-300 }); 
	
	$("#intro-ingresar").click(function(){
		$("#intro").popup("close");
	});
});

$(window).load(function(){
	$("#intro").popup("open");	
})

$(window).resize(function () {
	$('#map-canvas').css({ width: $(window).width(),  height: $(window).height()-50 }); 
	$('#section-gz').css({ height: $(window).height()-300 }); 
});


		
function initialize() {

	var mapOptions = {
		center: new google.maps.LatLng(-12.111587, -77.0118652),
		zoom: 14,
		panControl: true,
		panControlOptions: {
		  position: google.maps.ControlPosition.TOP_RIGHT
		},
		zoomControl: true,
		zoomControlOptions: {
		  style: google.maps.ZoomControlStyle.LARGE,
		  position: google.maps.ControlPosition.TOP_RIGHT
		}
	
	};

	var map = new google.maps.Map(document.getElementById("map-canvas"),
		mapOptions);
	
	
	 var contentString = '<div class="marker-content">'+
		  '<div class="marker-wrap-image">'+
		  '<img class="marker-image" src="images/consultoras/img1.jpg">'+
		  '</div>'+
		  '<div class="marker-description">'+
		  '<h1 class="marker-name">Ángela Romero del Monte Prado</h1>'+
		  '<p>'+
		  'Dirección: Av. La Encalada 1448, Monterrico, Santiago de Surco<br/><br/>' +
		  'Teléfono de casa: 01 364 8795<br/><br/>'+
		  'Móvil: 987 654 321<br/>'+
		  '</p>'+
		  '<p>'+
		  '<div>'+
		  '<a href="#positionWindow" class="ui-btn" data-rel="popup" data-position-to="window">Inscribirse</a>'+
		  '<div>'+
		  '</p>'+
		  '</div>';
	
	  var infowindow = new google.maps.InfoWindow({
		  content: contentString
	  });
	
	  var marker = new google.maps.Marker({
		  position: new google.maps.LatLng(-12.111587, -77.0118652),
		  map: map,
		  title: 'Ángela Romero del Monte Prado'
	  });
	  google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(map,marker);
	  });
	
}
google.maps.event.addDomListener(window, 'load', initialize);